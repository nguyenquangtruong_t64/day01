-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 11, 2022 lúc 04:14 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlsv`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dmkhoa`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `dmkhoa`
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('1', 'Cong nghe thong tin'),
('2', 'Tai nguyen moi truong'),
('3', 'Khoa hoc du lieu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sinhvien`
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) DEFAULT NULL,
  `HocBong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('', NULL, NULL, NULL, '2022-09-20 00:00:00', NULL, NULL, NULL, NULL),
('101', 'Nguyen Van', 'A', '1', '2022-09-01 00:00:00', 'QN', 'Ha Long', '1', 200000),
('102', 'Nguyen Van', 'B', '0', '0000-00-00 00:00:00', 'QN', 'Ha Long', '1', 200000),
('103', 'Nguyen Van', 'C', '1', '2022-08-09 00:00:00', 'QN', 'Ha Long', '3', 200000),
('104', 'Nguyen Van', 'D', '1', '2022-09-02 00:00:00', 'QN', 'Ha Long', '2', 200000),
('105', 'Nguyen Van', 'E', '1', '2022-08-09 00:00:00', 'QN', 'Ha Long', '1', 200000),
('106', 'Nguyen Van', 'F', '0', '2022-08-10 00:00:00', 'QN', 'Ha Long', '1', 200000),
('107', 'Nguyen Van', 'G', '1', '2022-08-15 00:00:00', 'QN', 'Ha Long', '2', 200000),
('108', 'Nguyen Van', 'H', '1', '2022-09-06 00:00:00', 'QN', 'Ha Long', '2', 200000),
('109', 'Nguyen Van', 'I', '0', '2022-08-16 00:00:00', 'QN', 'Ha Long', '3', 200000),
('110', 'Nguyen Van', 'J', '0', '2022-08-25 00:00:00', 'QN', 'Ha Long', '2', 200000),
('111', 'Nguyen Van', 'K', '1', '2022-08-30 00:00:00', 'QN', 'Ha Long', '3', 200000);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `dmkhoa`
--
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);

--
-- Chỉ mục cho bảng `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
